import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule, routingComponents} from './app-routing.module';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { ContentComponent } from './components/content/content.component';
import { FooterComponent } from './components/footer/footer.component';
import { DirectivaIfComponent } from './components/directiva-if.component';
import { DirectivaForComponent } from './components/directiva-for.component';
import { DirectivaSwitchComponent } from './components/directiva-switch.component';
import { DataService } from '../app/datos.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ContentComponent,
    FooterComponent,
    DirectivaIfComponent,
    DirectivaForComponent,
    DirectivaSwitchComponent,
    routingComponents
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [
    DataService 
  ],
  bootstrap: [AppComponent]
  
})
export class AppModule { }
