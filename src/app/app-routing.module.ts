import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ComputadoresComponent } from './computadores/computadores.component';

const routes: Routes = [
    { path: 'computadores', component: ComputadoresComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule { }
export const routingComponents = [ComputadoresComponent]