import { Component } from '@angular/core';

@Component({
    selector: 'miContent',
    templateUrl: './content.component.html',
    styleUrls:['./content.component.css']

})

export class ContentComponent{

    datosPag = {
        imgPag: '../assets/samsung.jpg',
        imgPag2: '../assets/huawei-smartwatch.jpg',
        imgPag3: '../assets/macbook.jpg',

        infoPag: 'Ropa con estilo y calidad',
    }
    
    chat = {
        data: ''
    }
}