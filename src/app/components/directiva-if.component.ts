import { Component } from '@angular/core';

@Component({
    selector: 'directiva-if',
    template: `
    <div *ngIf= "Cargar">
    <h1>Bienvenidos al mejor lugar en tecnología</h1>
    </div>
    <style>
    h1{
        text-align: center;
    }
    </style>
    ` 
    
})

export class DirectivaIfComponent{
    Cargar: Boolean = true;
}