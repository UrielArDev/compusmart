import { Component } from '@angular/core';
import { DataService } from './datos.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'pagina-angular';
  datosAlumnos: any=[];

  constructor(private dataservice: DataService){
    this.datosAlumnos = dataservice.obtenerAlumnos()
  }
}
